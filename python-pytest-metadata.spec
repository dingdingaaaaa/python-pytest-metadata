%global _empty_manifest_terminate_build 0
Name:		python-pytest-metadata
Version:	2.0.4
Release:	1
Summary:	pytest plugin for test session metadata
License:	Mozilla Public License 2.0 (MPL 2.0)
URL:		https://github.com/pytest-dev/pytest-metadata
Source0:	https://files.pythonhosted.org/packages/4e/57/e26bd9b5caba5bfdb6b2916fcc875018c1c6ee80b159aee7acb19f8a81f1/pytest_metadata-2.0.4.tar.gz
BuildArch:	noarch
%description
pytest plugin for test session metadata


%package -n python3-pytest-metadata
Summary:	pytest plugin for test session metadata
Provides:	python-pytest-metadata
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-pbr
BuildRequires:  python3-setuptools_scm
Requires:	python3-pytest
%description -n python3-pytest-metadata
pytest plugin for test session metadata


%package help
Summary:	Development documents and examples for pytest-metadata
Provides:	python3-pytest-metadata-doc
%description help
pytest plugin for test session metadata


%prep
%autosetup -n pytest_metadata-2.0.4

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pytest-metadata -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Dec 07 2022 dingdingaaaaa <dingziwei@kylinos.cn> - 2.0.4-1
- Upgrade version to 2.0.4

* Fri Jul 09 2021 openstack-sig <openstack@openeuler.org>
- Package Spec generated
